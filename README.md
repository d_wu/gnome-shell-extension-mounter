# Mounter
Mount and umount fstab entries with 'noauto,user' options.

/etc/fstab example that will be picked up and listed by the extension:

```
192.168.1.1:/mnt/data/users/martin  /mnt/server-martin  nfs  noauto,user,noatime,rw  0  0
```

Toggle the menu with the shortcut: CTRL + ALT + m

## Installation
```bash
$ cd ~/.local/share/gnome-shell/extensions
$ git clone https://gitlab.com/d_wu/gnome-shell-extension-mounter mounter@heartmire
```

## Screenshot
![Screenshot](https://gitlab.com/martinhjartmyr/gnome-shell-extension-mounter/-/raw/master/mounter.png)
