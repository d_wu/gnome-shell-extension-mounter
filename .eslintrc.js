module.exports = {
  "extends": "eslint:recommended",
  "env": {
    "es6": true
  },

  "parserOptions": {
    "sourceType": "module",
    "ecmaVersion": 2017,
  },

  "globals": {
    "imports": "readonly",
    "console": "readonly",
  },

  "rules": {
    "object-shorthand": "error",
    "space-before-function-paren": ["error", "never"],
    "prefer-const": "error",
    "no-underscore-dangle": "off",
    "brace-style": ["error"],
    "no-unused-vars": ["error", {
        "vars": "local",
        "args": "none",
        "varsIgnorePattern": "(init|enable|disable|buildPrefsWidget|[A-Z])"
    }],
    "quotes": ["error", "double"],
    "prefer-template": "off",
    "comma-spacing": "error",
    "max-len": ["error", 100]
  }
}
