"use strict";

import Clutter from "gi://Clutter";
import Gio from "gi://Gio";
import GioUnix from "gi://GioUnix";
import GLib from "gi://GLib";
import GObject from "gi://GObject";
import Meta from "gi://Meta";
import Shell from "gi://Shell";
import St from "gi://St";

import { Extension } from "resource:///org/gnome/shell/extensions/extension.js";
import * as PanelMenu from "resource:///org/gnome/shell/ui/panelMenu.js";
import * as PopupMenu from "resource:///org/gnome/shell/ui/popupMenu.js";

import * as Main from "resource:///org/gnome/shell/ui/main.js";
import * as Util from "resource:///org/gnome/shell/misc/util.js";

const ICON_SIZE = 16;
const MOUNTER_ICON = "drive-harddisk-symbolic";
const MOUNT_ICON = "drive-harddisk-symbolic";
const UMOUNT_ICON = "media-eject-symbolic";
const OPEN_FOLDER_ICON = "folder-open-symbolic";

const FSTAB = "/etc/fstab";
const MTAB = "/etc/mtab";

// Settings (schema) names
const SCHEMA_ENABLE_KEYBINDING = "mounter-enable-keybinding";
const SCHEMA_TOGGLE_MENU = "mounter-toggle-menu";

const MounterIndicator = GObject.registerClass(
class MounterIndicator extends PanelMenu.Button {
  _init(_mounter) {
    super._init(0.0, "MounterIndicator");

    this._mounter = _mounter;
    this._shortcutId = null;

    const icon = new St.Icon({icon_name: MOUNTER_ICON, style_class: "system-status-icon"});
    this.add_child(icon);
    this.buildMenu();

    if (this._mounter.enableShortcut) {
      this._bindShortcut();
    }
  }

  toggleMenu() {
    this.menu.toggle();
  }

  buildMenu() {
    const menu = this.menu;
    menu.removeAll();
    this._mounter.mountsAvail.forEach(mount => {
      const icon_name = mount.mounted ? UMOUNT_ICON : MOUNT_ICON;
      const item = new PopupMenu.PopupImageMenuItem(mount.mountPoint, icon_name);
      item.connect(
        "activate", function(mount) {
          menu.close();
          this._mounter.onAction(item, mount, false);
        }.bind(this, mount)
      );
      menu.addMenuItem(item);

      const openFolderIcon = new St.Icon({
        icon_name: OPEN_FOLDER_ICON,
        style_class: "system-status-icon",
        icon_size: ICON_SIZE,
      });
      const open_folder_button = new St.Button({
        style_class: "ci-action-btn",
        can_focus: true,
        child: openFolderIcon,
        x_align: Clutter.ActorAlign.END,
        x_expand: true,
        y_expand: true
      });
      open_folder_button.connect(
        "clicked", function(mount) {
          menu.close()
          this._mounter.onAction(item, mount, true);
        }.bind(this, mount)
      );
      item.actor.add_child(open_folder_button);
    });
  }

  destroy() {
    this._unbindShortcut();
  }

  _bindShortcut() {
    const name = SCHEMA_TOGGLE_MENU;
    this._shortcutId = name;

    Main.wm.addKeybinding(
      name,
      this._mounter.extension.getSettings(),
      Meta.KeyBindingFlags.NONE,
      Shell.ActionMode.ALL,
      this.toggleMenu.bind(this)
    );
  }

  _unbindShortcut() {
    if (this._shortcutId != null) {
      Main.wm.removeKeybinding(this._shortcutId)
    }

    this._shortcutId = null;
  }
});

class Mounter {
  constructor(extension) {
    this.mountsAvail = []; // Contains all mounts found in fstab
    this.extension = extension;
    this.enableShortcut = extension.getSettings().get_boolean(SCHEMA_ENABLE_KEYBINDING);

    this._createMountMonitor();
    this._readFstab();
    this._readMounts();
  }

  // Callback from panelMenu item click
  async onAction(_item, _mount, openFolder) {
    if (_mount.mounted) {
      if (openFolder) {
        this.openFolder(_mount);
      } else {
        const result = await this.runCommandSync(["umount", _mount.mountPoint]);
        if (result.success) {
          _mount.mounted = false;
          _item.setIcon(MOUNT_ICON);
        } else {
          Main.notifyError(`${this.extension.metadata.name} Error`, result.stderr);
        }
      }
    } else {
      const result = await this.runCommandSync(["mount", _mount.mountPoint]);
      if (result.success) {
        _mount.mounted = true;
        _item.setIcon(UMOUNT_ICON);
      } else {
        Main.notifyError(`${this.extension.metadata.name} Error`, result.stderr);
      }
      if (openFolder) {
        this.openFolder(_mount);
      }
    }
  }

  openFolder(_mount) {
    const uri = GLib.filename_to_uri(_mount.mountPoint, null);
    Gio.AppInfo.launch_default_for_uri_async(uri, null, null, null);
  }

  async runCommandSync(command) {
    const proc = Gio.Subprocess.new(
      command,
      Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE);

    const [success, stdout, stderr] = await proc.communicate_utf8(null, null);
    return { success, stdout, stderr };
  }

  destroy() {
    if (this._mountsChangedId) {
      this._monitor.disconnect(this._mountsChangedId);
      this._mountsChangedId = 0;
    }
  }

  // Listen to system mount events
  _createMountMonitor() {
    this._monitor = GioUnix.MountMonitor.get();
    this._mountsChangedId = this._monitor.connect("mounts-changed", () => {
      this._readMounts();
    });
  }

  // Only matches mounts with the options "noauto" and "user"
  _readFstab() {
    const [success, content] = GLib.file_get_contents(FSTAB);

    if (success) {
      const contentLines = imports.byteArray.toString(content);
      contentLines.split("\n").forEach(line => {
        const data = line.match(/\S+/g) || [];
        if (data.length == 6 &&
            data[3].match("noauto") !== null &&
            data[3].match("user") !== null) {
          const mountEntry = {
            mounted: false,
            device: data[0],
            mountPoint: data[1],
          };
          this.mountsAvail.push(mountEntry);
        }
      });
    }
  }

  // Read currently mounted entries
  _readMounts() {
    const [success, content] = GLib.file_get_contents(MTAB);

    if (success) {
      const contentLines = imports.byteArray.toString(content);
      this.mountsAvail.forEach(mount => {
        mount.mounted = (contentLines.indexOf(mount.mountPoint) === -1) ? false : true;
      });
    }

    // Rebuild the panel menu based on new values
    if (this._indicator) {
      this._indicator.buildMenu();
    }
  }
}

let _mounter;

export default class MounterExtension extends Extension {
enable() {
  console.debug(`Enabling ${this.metadata.name} version ${this.metadata.version}`);
  _mounter = new Mounter(this);
  if (!this._indicator) {
    this._indicator = new MounterIndicator(_mounter);
    Main.panel.addToStatusArea(this.uuid, this._indicator);
  }
}

disable() {
  console.debug(`Disabling ${this.metadata.name} version ${this.metadata.version}`);
  this._indicator?.destory();
  this._indicator = null;

  _mounter?.destroy()
  _mounter = null;
}
}
